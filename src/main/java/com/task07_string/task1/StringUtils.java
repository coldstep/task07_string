package com.task07_string.task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {

  public static String concatObjects(Object... obj) {
    return Stream.of(obj).map(Object::toString).collect(Collectors.joining());
  }


  public static void checkSentence(String sentence) {
    String pattern1 = "\\w[^\\p{Punct}]+ ";
    String pattern2 = "( the | The | you | You )+";
    String pattern3 = "[aeiouAIEOU]+";

    System.out.println("1. Split array to sentences");

    Pattern p = Pattern.compile(pattern1);
    Matcher matcher = p.matcher(sentence);

    while (matcher.find()) {
      System.out.println(sentence.substring(
          matcher.start(), matcher.end()) + "*"
      );
    }
    System.out.println("2. Split some string on the words 'the' or 'you'");
    System.out.println(sentence.replaceAll(pattern2,"\n"));
    System.out.println("3. Replace all the vowels in some text with underscores.");
    System.out.println(sentence.replaceAll(pattern3,"_"));

  }
}
