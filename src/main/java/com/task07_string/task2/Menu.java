package com.task07_string.task2;

import static com.task07_string.task1.StringUtils.checkSentence;
import static com.task07_string.task1.StringUtils.concatObjects;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Menu {

  private Locale locale;
  private ResourceBundle resourceBundle;

  public Menu() {
    locale = new Locale("en");
  }

  public void show() {
    Map<String, Runnable> map = functionCompotent();
    Scanner scanner = new Scanner(System.in);
    while (true) {
      System.out.println(textCompotent());
      map.get(scanner.next().toLowerCase()).run();
    }

  }

  private String textCompotent() {
    StringBuilder stringBuilder = new StringBuilder();

    resourceBundle = ResourceBundle.getBundle("menu", locale);

    stringBuilder.append(resourceBundle.getString("1")).append("\n");
    stringBuilder.append(resourceBundle.getString("2")).append("\n");
    stringBuilder.append(resourceBundle.getString("3")).append("\n");
    stringBuilder.append(resourceBundle.getString("4")).append("\n");
    stringBuilder.append(resourceBundle.getString("Q")).append("\n");

    return stringBuilder.toString();

  }

  private Map functionCompotent() {
    Map<String, Runnable> functionMenu = new LinkedHashMap<>();
    functionMenu.put("1", this::testStringUtils);
    functionMenu.put("2", this::convertToChiness);
    functionMenu.put("3", this::convertToEnglish);
    functionMenu.put("4", this::testRegEx);
    functionMenu.put("q", this::goodbye);
    return functionMenu;
  }

  private void testStringUtils() {
    String text = "Welcome ! To the new world !! After this . You wil be my hero . ";

    checkSentence(text);

    String a = "hello worl";
    Character b = 'd';
    String c ="!!";
    Integer i = 220;
    System.out.println("Result of concating");
    System.out.println(concatObjects(a,b, c, i));


  }

  private void convertToEnglish() {
    locale = new Locale("en");
  }

  private void convertToChiness() {
    locale = new Locale("ja");
  }

  private void testRegEx() {

  }

  private void goodbye() {
    System.exit(0);
  }


}
