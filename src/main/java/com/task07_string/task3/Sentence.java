package com.task07_string.task3;

public class Sentence {

  private final String sentencePattern = "[\\w^\\p{Punct}]+";

  public String getSentencePattern() {
    return sentencePattern;
  }
}
