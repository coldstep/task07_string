# Task

1. Create class StringUtils with an undefined number of parameters of any class that concatenates all parameters and returns Strings.
2. Internationalize menu in your program for a few languages. https://native2ascii.net/
3. Using the documentation for java.util.regex.Pattern as a resource, write and test a regular expression that checks a sentence to see that it begins with a capital letter and ends with a period.
4. Split some string on the words "the" or "you".
5. Replace all the vowels in some text with underscores.

